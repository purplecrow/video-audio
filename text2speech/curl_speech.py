# coding=utf8

import requests
import json


def elasticCallResponse(ocr_text):

    uri = 'https://texttospeech.googleapis.com/v1/text:synthesize?key=AIzaSyDOx5P-vDaSiAYZGd_lMB_CNW_5McJvVAU'
    payload ={
        "input":{
            "text":"चूँकि फंक्शन f और G इंटरवल 014 पर डिसेबल होते हैं, एक्स का कोई भी फंक्शन जो फंक्शन fx का एक लीनियर कॉम्बिनेशन है और इंटरवल 0 1 पर एक डिफरेंशियल कॉम्बिनेशन है।"
        },
        "voice":{
            "languageCode":"hi-IN",
            "name":"hi-IN-Wavenet-D",
            "ssmlGender":"FEMALE"
        },
        "audioConfig":{
            # "audioEncoding":"MP3"
            "audioEncoding": "LINEAR16",
            "pitch": 0,
            "speakingRate": 1
        }
        }
    headers = {
        'Content-Type': 'application/json'
    }

    response  = requests.post(uri, data=json.dumps(payload), headers=headers)
    return response.json()


response = elasticCallResponse("ok http")

print(response.get('audioContent'))

# print(elasticCallResponse("ok http"))



