# coding=utf8
import requests
import json



uri = 'https://texttospeech.googleapis.com/v1/text:synthesize?key=AIzaSyDOx5P-vDaSiAYZGd_lMB_CNW_5McJvVAU'


payload = {
  "audioConfig": {
    "audioEncoding": "LINEAR16",
    "pitch": 0,
    "speakingRate": 1
  },
  "input": {
    "text": "આ ત્રણ સેટનું ઉદાહરણ શોધવા માટે છે"
  },
  "voice": {
    "languageCode": "gu-IN",
    "name": "gu-IN-Standard-A"
  }
}

headers = {
        'Content-Type': 'application/json'
    }


response  = requests.post(uri, data=json.dumps(payload), headers=headers)
print(response.json())
content = response.json().get('audioContent')

file = open("resp_content8.txt", "w")
file.write(content)
file.close()
