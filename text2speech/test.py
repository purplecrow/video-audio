import texttospeech as tts


def list_languages():
    client = tts.TextToSpeechClient()
    voices = client.list_voices().voices
    languages = unique_languages_from_voices(voices)

    print(f' Languages: {len(languages)} '.center(60, '-'))
    for i, language in enumerate(sorted(languages)):
        print(f'{language:>10}', end='' if i % 5 < 4 else '\n')

list_languages()

def unique_languages_from_voices(voices):
    language_set = set()
    for voice in voices:
        for language_code in voice.language_codes:
            language_set.add(language_code)
    return 