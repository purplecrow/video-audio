import re
import sys
import os
import speech_recognition as sr
import urllib.request


# sample - video - url   :-  https://d3cvwyf9ksu0h5.cloudfront.net/XI_01_MEX_15.mp4


base_url  = 'http://d3cvwyf9ksu0h5.cloudfront.net/'


answer_video_url = sys.argv[1]
print('answer_video_url')
print(answer_video_url)

answer_video = re.sub(base_url,"",answer_video_url)
filename = answer_video.split('.')[0]
print('answer video')
print(answer_video)

local_video = urllib.request.urlretrieve(answer_video_url, './videos/{}'.format(answer_video))


# got the video -  retireve audio now

command2mp3 = """ffmpeg -i /Users/akshatsandhaliya/Desktop/video-audio/final/videos/{}.mp4  {}.mp3""".format(filename,filename)
command2wav = """ffmpeg -i {}.mp3 {}.wav""".format(filename,filename)


os.system(command2mp3)
os.system(command2wav)

# os.system(cmd)

r = sr.Recognizer()

audio = sr.AudioFile("{}.wav".format(filename))
# hellow=sr.AudioFile('hello_world.wav')
with audio as source:
    r.adjust_for_ambient_noise(source, duration=0.5)
    audio = r.record(source)


print(r.recognize_google(audio, language="en"))


