import os
import pyglet
from gtts import gTTS 

language = 'en'

mytext = 'Welcome to Doubtnut'

myobj = gTTS(text=mytext, lang=language, slow=False) 

myobj.save("welcome.mp3") 


music = pyglet.resource.media('welcome.mp3')
music.play()

pyglet.app.run()
# os.system("mpg321 welcome.mp3") 
