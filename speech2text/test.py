# coding=utf8
from google.cloud import speech
import requests
import json



# speech_context = speech.types.SpeechContext(phrases=['$TIME'])


# uri = 'https://speech.googleapis.com/v1/speech:longrunningrecognize?key=AIzaSyDKkroJWYjdmI_InLu-Vbs4d1Fe02iQuXo'
uri = 'https://speech.googleapis.com/v1/speech:recognize?key=AIzaSyDKkroJWYjdmI_InLu-Vbs4d1Fe02iQuXo'

# AIzaSyDOx5P-vDaSiAYZGd_lMB_CNW_5McJvVAU
# AIzaSyDKkroJWYjdmI_InLu-Vbs4d1Fe02iQuXo
payload = {
  "config": {
      "encoding":"LINEAR16",
      "languageCode": "en-US",
      "enableWordTimeOffsets": True,
      "maxAlternatives" :10,
    #   "enableAutomaticPunctuation":True
    #    "speechContexts":[{
    #   "phrases": ["fair"],
    #   "boost": 15
    #  }, {
    #   "phrases": ["fare"],
    #   "boost": 2
    #  }]
  },
  "audio": {
      "uri" : "gs://doutbnut_sounds/answer-1495614002_new.wav"
    #   "uri":"gs://cloud-samples-tests/speech/brooklyn.flac"
  }
}

# }

# speech:longrunningrecognize
# payload = {
#   "audioConfig": {
#     "audioEncoding": "LINEAR16",
#     "pitch": 0,
#     "speakingRate": 1
#   },
#   "input": {
#     "text": "यह तो तीन सेटों का एक उदाहरण खोजने के लिए है जैसे कि एक चौराहा b चौराहा c और एक गंभीर गैर खाली सेट में प्रवेश करता है और तीनों सेटों का प्रतिच्छेदन एक संपत्ति है और यादृच्छिक उदाहरण के रूप में 12349 BS 4 5 6 7 8 और cs7 89012 लिया था यादृच्छिक पर चुना गया है इसलिए एक चौराहा है बी चौराहे बी एंड्रॉइड के लिए सिर्फ तत्व हैं बी चौराहे सीबी चौराहे श्रृंखला 7 और 8 के तत्वों की श्रृंखला क्या है और सी चौराहे बी चौराहे कौन से तत्व 1 और 2 हैं और एक चौराहे बी चौराहे क्या हैं यही कारण है कि सभी तीनों तत्वों में से कोई भी तत्व सामान्य है इसलिए एक खाली सेट है या मैं इसे एक अशक्त सेट के रूप में भी उपयोग कर सकता हूं, इसलिए यह एक उदाहरण है जो उन्हें वहां मिला है ab और c ऐसे हैं जो ये तीन पक्ष खाली नहीं हैं और यह सेट खाली है हम असीम रूप से कई अन्य संयोजन पा सकते हैं जो मैं सिर्फ 1 परीक्षा देता हूं"
#   },
#   "voice": {
#     "languageCode": "hi-IN",
#     "name": "hi-IN-Wavenet-D"
#   }
# }

headers = {
        'Content-Type': 'application/json'
    }


response  = requests.post(uri, data=json.dumps(payload), headers=headers)
print(response.json())
# content = response.json().get('audioContent')

# file = open("resp_content7.txt", "w")
# file.write(content)
# file.close()
